include .env

CONTAINER_NAME=dbeaver-${VERSION}
IMAGE_NAME=dbeaver:local-${VERSION}
NETWORK=db-shared
UID=$(shell id -u)

build:
	docker build . --tag ${IMAGE_NAME} --build-arg VERSION=${VERSION} --build-arg UID=${UID}

create:
	@# See: https://www.youtube.com/watch?v=Jp58Osb1uFo
	@-docker network create ${NETWORK}
	@-docker rm -f ${CONTAINER_NAME}
	docker create \
		--name ${CONTAINER_NAME} \
		--network ${NETWORK} \
		--user dbeaver \
		--volume $(shell readlink -f ~/.local/share/fonts/)/:/home/dbeaver/.local/share/fonts/ \
		--volume $(shell readlink -f ./volumes/dbeaver)/:/home/dbeaver/ \
		--volume /tmp/.X11-unix/:/tmp/.X11-unix/:ro \
		-e DISPLAY="${DISPLAY}" ${IMAGE_NAME}

run:
	# FIXME: ** (dbeaver:6): WARNING **: Couldn't connect to accessibility bus: Failed to connect to socket /tmp/dbus-UuKZYFYzHS: Connection refused
	docker start ${CONTAINER_NAME}
	docker logs ${CONTAINER_NAME}

install:
	sudo cp ./dbeaver-dkr.desktop /usr/share/applications/dbeaver-dkr.desktop
	sudo sed -i -r "s/\{\{VERSION\}\}/${VERSION}/" /usr/share/applications/dbeaver-dkr.desktop

all:
	$(MAKE) build
	$(MAKE) create
	$(MAKE) run

console:
	docker run -it --rm ${IMAGE_NAME} bash
