# Dkr-DBeaver

Dockerized DBeaver to allow mnemonic database access to running db containers

### Quickstart

`cp .env.example .env`
`make all`

then join each docker container you want to access from DBeaver (by container name) to the network `db-shared`

Data such as preferences and queries will be stored under `./volumes/dbeaver`

### Gnome integration

`make install`

### Troubleshooting

Changin display adapter may require a re-creation of the container because of a change of `DISPLAY` variable; in such case fix by issuing `make create`

