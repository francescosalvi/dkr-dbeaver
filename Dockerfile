# Source: https://github.com/jainishshah17/docker-dbeaver/blob/master/Dockerfile

FROM java:openjdk-8-jre

ARG VERSION
ARG UID

RUN apt-get update; exit 0
RUN apt-get install -y libswt-gtk-3-jni libswt-gtk-3-java libcanberra-gtk-module libcanberra-gtk3-module

ADD https://github.com/dbeaver/dbeaver/releases/download/${VERSION}/dbeaver-ce_${VERSION}_amd64.deb .
RUN dpkg -i dbeaver-ce_${VERSION}_amd64.deb

RUN mkdir /home/dbeaver && \
    useradd -u ${UID} dbeaver && \
    chown dbeaver:dbeaver /home/dbeaver

VOLUME /home/dbeaver

CMD dbeaver